var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});



app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje osebe)
 */
app.get('/api/dodaj', function(req, res) {
	// ...
	res.send('Potrebno je implementirati dodajanje oseb!');
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje osebe)
 */
app.get('/api/brisi', function(req, res) {
	// ...
	//res.send('Potrebno je implementirati brisanje oseb!');
	
	var davSt = req.query.id;
	
	console.log(davSt);
	
	if(davSt == null || davSt.length == 0) {
		res.send("Napačna zahteva!");
	}
	
	for (var i in uporabnikiSpomin) {
		if (uporabnikiSpomin[i]["davcnaStevilka"] == davSt) {
			uporabnikiSpomin.splice(i, 1);
			res.redirect("/");
		}
	}
	
	res.send("Oseba z davčno številko " + davSt + " ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
	
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];